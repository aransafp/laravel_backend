<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\productSKU;
use Illuminate\Support\Facades\Auth;

class ProductSKUController extends Controller
{
    public function insert_sku(Request $request)
    {
        if (Auth::user()->role == 1){
            $cek_sku = productSKU::where('id_product', $request->id_product)->where('sku_code', $request->sku_code)->first();
            
            if($cek_sku == null){
                $sku = productSKU::create([
                    'id_product' => $request->id_product,
                    'sku_code' => $request->sku_code
                ]);
                return response(['message' => 'Success insert SKU in product']);
            } 
            
            return response(['message' => 'SKU code in '.Product::find($cek_sku->id_product)->name.' already exist']);
        } else {
            return response(['message' => 'Only Admin can do this']);    
        }
    }
    
    public function get_by_product_sku(Request $request){
        if (Auth::user()->role == 1){
            $sku = productSKU::where('sku_code', $request->sku_code)->get();
            return response(['message' => 'Success get SKU by product SKU', 'sku_product' => $sku]);    
        } else {
            return response(['message' => 'Only Admin can do this']);    
        }
    }
    
    public function get_all_product_sku(Request $request){
        if (Auth::user()->role == 1){
            $sku = productSKU::all();
            return response(['message' => 'Success get all SKU', 'list_sku_product' => $sku]);    
        } else {
            return response(['message' => 'Only Admin can do this']);    
        }
    }
    
    public function get_all_sku_product(Request $request){
        if (Auth::user()->role == 1){
            $sku = productSKU::where('id_product', $request->id_product)->get();
            return response(['message' => 'Success get SKU in product', 'list_sku_code' => $sku]);    
        } else {
            return response(['message' => 'Only Admin can do this']);    
        }
    }
    
    public function delete_sku(Request $request){
        if (Auth::user()->role == 1){
            productSKU::where('id_product', $request->id_product)->where('sku_code', $request->sku_code)->delete();
            return response(['message' => 'Success delete SKU in product']);    
        } else {
            return response(['message' => 'Only Admin can do this']);    
        }
    }
}
