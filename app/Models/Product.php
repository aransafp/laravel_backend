<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'desc',
        'base_price',
        'publish_price',
        'tinggi',
        'berat',
        'warna',
        'jenis',
        'stok',
        'diskon',
        'timestamp'
    ];

    protected $hidden = [
        'timestamp'
    ];
}
